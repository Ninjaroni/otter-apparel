import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import About from '@/components/About'
import Products from '@/components/Products'
import WomensHome from '@/components/Womens-Home'
import MensHome from '@/components/Mens-Home'
import KidsHome from '@/components/Kids-Home'
import SalesHome from '@/components/Sales-Home'
import BrandsHome from '@/components/Brands-Home'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'homepage',
            component: Homepage
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/products/:category/:type',
            name: 'products',
            component: Products
        },
        {
            path: '/womens-home',
            name: 'womens-home',
            component: WomensHome
        },
        {
            path: '/mens-home',
            name: 'mens-home',
            component: MensHome
        },
        {
            path: '/kids-home',
            name: 'kids-home',
            component: KidsHome
        },
        {
            path: '/sales-home',
            name: 'sales-home',
            component: SalesHome
        },
        {
            path: '/brands-home',
            name: 'brands-home',
            component: BrandsHome
        },
    ],
    mode: 'history'
})

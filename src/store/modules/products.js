
// initial state
const state = {
    products: {
        womens:{
            jeans: [
                {
                    id: 1234,
                    name: "proper good jeans",
                    value: 5.50
                }
            ]
        }
    }
}

// getters
const getters = {
    allProducts: state => state.products,
}

// actions
const actions = {
    init ({ commit }, routeParams) {
        //TODO: Do something about getting the correct category and type from state.
    },
    getAllProducts ({ commit }) {
        commit('setProducts', products)
    }
}

// mutations
const mutations = {
    // setProducts (state, products) {
    //     state.all = products
    // },
    // decrementProductInventory (state, { id }) {
    //     const product = state.all.find(product => product.id === id)
    //     product.inventory--
    // }
}

export default {
    state,
    getters,
    actions,
    mutations
}